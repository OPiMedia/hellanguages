#!/bin/bash

#
# Simple try to read multiples files line by line.
# Usage: reads [filename1 ...]
# Run with text files examples: ./reads.sh examples/*.txt
#
# Piece of hellanguages.
# https://bitbucket.org/OPiMedia/hellanguages
#
# GPLv3 --- Copyright (C) 2022 Olivier Pirson
# http://www.opimedia.be/
#

function reads {  # [filename1 ...]
    declare -a files
    local -i file

    # Open files
    for filename in "$@"; do
        file=0
        if [[ -e "$filename" ]]; then
            if exec {file}< "$filename" 2> /dev/null; then
                echo "Opened |$file|$filename|"
                files+=("$file")
            else
                echo "Can't open file \"$filename\"!"
            fi
        else
            echo "File \"$filename\" doesn't exist!"
        fi
    done


    # Read files
    local line
    local -i not_end=0

    IFS=''
    while [[ $not_end -eq 0 ]]; do
        not_end=1
        for file in "${files[@]}"; do
            if read -u "$file" -r line; then
                not_end=0
                echo "Read |$file|$line|"
            fi
        done
    done


    # Close files
    for file in "${files[@]}"; do
        exec {file}>&-
        echo "Closed |$file|"
    done
}



reads "$@"
