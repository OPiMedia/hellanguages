/* -*- coding: latin-1 -*- */
/*
 * iteration.c (January 5, 2020)
 *
 * Time benchmarking of (empty) iteration from 0 to 2**MAX_SIZE.
 *
 * Piece of hellanguages.
 * https://bitbucket.org/OPiMedia/hellanguages
 *
 * GPLv3 --- Copyright (C) 2017, 2020 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>


int
main(int argc, const char* const argv[]) {
  const unsigned int NB_ITER = (argc > 1 ? atoi(argv[1]) : 10);
  const unsigned int MAX_SIZE = (argc > 2 ? atoi(argv[2]) : 36);

  unsigned int size;

  if (sizeof(unsigned int) != 4) {
    printf("! sizeof(unsigned int) == %lu != 4", sizeof(unsigned int));

    return EXIT_FAILURE;
  }
  if (sizeof(unsigned long) != 8) {
    printf("! sizeof(unsigned long) == %lu != 8", sizeof(unsigned long));

    return EXIT_FAILURE;
  }


  puts("# bits\tseconds");


  /* For each sizes to min(31, MAX_SIZE) */
  for (size = 1; (size <= 31) && (size <= MAX_SIZE); ++size) {
    const unsigned int until = 1u << size;

    printf("%u", size);
    fflush(stdout);

    const clock_t start = clock();

    for (unsigned int iter = 0 ; iter < NB_ITER; ++iter) {
      /* Iterates from 0 until 2^size */
      for (unsigned int i = 0 ; i < until; ++i) {
        /* nothing */
      }
    }

    const clock_t duration = clock() - start;

    printf("\t%f\n", ((double)duration) / (CLOCKS_PER_SEC * NB_ITER));
    fflush(stdout);
  }


  /* For each sizes from 32 to MAX_SIZE */
  for (size = 32; size <= MAX_SIZE; ++size) {
    const unsigned int until = 1u << 31;
    const unsigned int nb_block = 1u << (size - 31);

    printf("%u", size);
    fflush(stdout);

    const clock_t start = clock();

    /* Iterates by block from 0 until 2^size */
    for (unsigned int block = 0 ; block < nb_block; ++block) {
      /* Iterates from 0 until 2^31 */
      for (unsigned int i = 0 ; i < until; ++i) {
        /* nothing */
      }
    }

    const clock_t duration = clock() - start;

    printf("\t%f\n", ((double)duration) / CLOCKS_PER_SEC);
    fflush(stdout);
  }

  return EXIT_SUCCESS;
}
