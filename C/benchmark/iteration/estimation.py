#!/usr/bin/env python
# -*- coding: latin-1 -*-

"""
estimation.py

:license: GPLv3 --- Copyright (C) 2020 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: January 5, 2020
"""

from __future__ import division
from __future__ import print_function

import sys


#
# Function
##########
def print_duration(size, duration):
    minute = duration / 60
    hour = minute / 60
    day = hour / 24
    year = day / 365.25
    print('{}\t{:20.3f} second\t{:20.3f} minute\t{:16.3f} hour\t{:16.3f} day\t{:16.3f} year'
          .format(size, duration, minute, hour, day, year))


#
# Main
######
def main():
    """
    Main
    """
    if len(sys.argv) < 3:
        print('estimation.py SIZE DURATION', file=sys.stderr)

        exit(1)

    size = int(sys.argv[1])
    duration = float(sys.argv[2])
    print_duration(size, duration)
    print('Estimation...')

    for i in range(size + 1, 65):
        duration *= 2
        print_duration(i, duration)


if __name__ == '__main__':
    main()
