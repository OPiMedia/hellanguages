unsigned int
shl(unsigned int n) {
  return n >> 1;
}
