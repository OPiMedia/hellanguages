/*
 * passing_multi_array.c (February 20, 2019)
 *
 * Several way to transmit 2D array to a function.
 *
 * Piece of hellanguages.
 * https://bitbucket.org/OPiMedia/hellanguages
 *
 * GPLv3 --- Copyright (C) 2015, 2019 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <stdio.h>
#include <stdlib.h>


#define HEIGHT 4
#define WIDTH 6



void
passing_2d(char a[HEIGHT][WIDTH]) {
  /* Only for array with exactly WIDTH columns and at least HEIGHT rows. */
  puts("=== print in passing_2d ===");
  for (unsigned int i = 0 ; i < HEIGHT; ++i) {
    for (unsigned int j = 0 ; j < WIDTH; ++j) {
      printf("%c", a[i][j]);
    }
    puts("");
  }

  a[1][2] = '_';  // modify the original array
}


void
passing_1d(char a[][WIDTH], unsigned int height) {
  /* Only the first dimension can be omitted
     because the compiler need to know other dimensions to compute [i][j].
     Equivalent to passing_1d(char (*a)[WIDTH], unsigned int height) */
  puts("=== print in passing_1d ===");
  for (unsigned int i = 0 ; i < height; ++i) {
    for (unsigned int j = 0 ; j < WIDTH; ++j) {
      printf("%c", a[i][j]);
    }
    puts("");
  }

  a[2][3] = '*';  // modify the original array
}


void
passing_ptr(char* pa, unsigned int height, unsigned int width) {
  /* Transmit explicitly a pointer. The compiler can't compute [i][j] anymore. */
  puts("=== print in passing_ptr ===");
  char* ptr = pa;
  for (unsigned int i = 0; i < height; ++i) {
    for (unsigned int j = 0 ; j < width; ++j, ++ptr) {
      printf("%c", *ptr);
    }
    puts("");
  }

  *(pa + width*3 + 4) = '@';  // modify the original array
}



int
main() {
  char a[HEIGHT][WIDTH] = {{'a', 'b', 'c', 'd', 'e', 'f'},
                           {'g', 'h', 'i', 'j', 'k', 'l'},
                           {'m', 'n', 'o', 'p', 'q', 'r'},
                           {'s', 't', 'u', 'v', 'w', 'x'}};

  passing_2d(a);
  passing_1d(a, HEIGHT);
  passing_ptr((char *)a, HEIGHT, WIDTH);

  puts("=== print in main ===");
  for (unsigned int i = 0 ; i < HEIGHT; ++i) {
    for (unsigned int j = 0 ; j < WIDTH; ++j) {
      printf("%c", a[i][j]);
    }
    puts("");
  }

  return EXIT_SUCCESS;
}
