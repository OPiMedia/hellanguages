signed int
cmp_ternary_operator(unsigned int a, unsigned b) {
  return (a > b
          ? 1
          : (a < b
             ? -1
             : 0));
}
