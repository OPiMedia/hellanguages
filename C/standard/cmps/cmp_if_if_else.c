signed int
cmp_if_if_else(unsigned int a, unsigned b) {
  if (a > b) {
    return 1;
  }
  else if (a < b) {
    return -1;
  }
  else {
    return 0;
  }
}
