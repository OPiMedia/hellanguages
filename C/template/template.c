/* -*- coding: latin-1 -*- */
/*
 * template.c (July 4, 2020)
 *
 * GPLv3 --- Copyright (C) 2020 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <stdio.h>
#include <stdlib.h>


int
main(int argc, const char* const argv[]) {
  printf("%i %s\n", argc, argv[0]);

  return EXIT_SUCCESS;
}
