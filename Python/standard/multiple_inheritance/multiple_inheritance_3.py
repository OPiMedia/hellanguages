#!/usr/bin/env python3
# -*- coding: latin-1 -*-

"""
multiple_inheritance.py

Show the behaviour of the dynamic dispatch
when a class inherit from two other classes.

Version for Python 3 only.

:license: GPLv3 --- Copyright (C) 2018, 2019 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: February 12, 2019
"""

from __future__ import division


class A:
    def __init__(self):
        print('class A: __init__\t', super())

    def foo(self):
        print('class A: foo\t\t', super())

    def call_foo(self):
        print('class A: call_foo\t', super())
        self.foo()


class B:
    def __init__(self):
        print('class B: __init__\t', super())

    def foo(self):
        print('class B: foo\t\t', super())


class AB(A, B):
    pass


class BA(B, A):
    pass


def main():
    print('=== class A ===')
    a = A()
    a.foo()
    a.call_foo()

    print('=== class B ===')
    b = B()
    b.foo()

    print('=== class AB(A, B) ===')
    ab = AB()
    ab.foo()       # call foo() from A
    ab.call_foo()  # call call_foo() and foo() from A

    print('=== class BA(B, A) ===')
    ba = BA()
    ba.foo()       # call foo() from A
    ba.call_foo()  # call call_foo() from A and foo() from B

if __name__ == '__main__':
    main()
