#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
template.

:license: GPLv3 --- Copyright (C) 2020 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version:
"""

#
# Functions
###########


#
# Main
######
def main() -> None:
    """Run main work."""


if __name__ == '__main__':
    main()
