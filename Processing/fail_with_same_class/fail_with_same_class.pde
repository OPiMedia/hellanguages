// In Processing you can define an "isolated" function.
// In fact it will be inserted as a method in the implicit class created by Processing.
// See build/source/fail_with_same_class.java
void foo() {
}



// But be careful, you can't create a class with the same name than this implicit class.
// Else the Java compilation fails: "The nested type fail_with_same_class cannot hide an enclosing type".
class fail_with_same_class {
}
