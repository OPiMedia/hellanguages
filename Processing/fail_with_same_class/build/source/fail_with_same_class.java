/* autogenerated by Processing revision 1292 on 2023-06-14 */
import processing.core.*;
import processing.data.*;
import processing.event.*;
import processing.opengl.*;

import java.util.HashMap;
import java.util.ArrayList;
import java.io.File;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.IOException;

public class fail_with_same_class extends PApplet {

// In Processing you can define an "isolated" function.
// In fact it will be inserted as a method in the implicit class created by Processing.
// See build/source/fail_with_same_class.java
public void foo() {
}



// But be careful, you can't create a class with the same name than this implicit class.
// Else the Java compilation fails: "The nested type fail_with_same_class cannot hide an enclosing type".
class fail_with_same_class {
}


  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "fail_with_same_class" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
