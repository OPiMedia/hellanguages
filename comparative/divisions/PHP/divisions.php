<?php

/* -*- coding: latin-1 -*- */
/*
 * Little tests of round behaviour and Euclidean division
 * in PHP.
 * https://www.php.net/
 *
 * Piece of hellanguages.
 * https://bitbucket.org/OPiMedia/hellanguages
 *
 * GPLv3 --- Copyright (C) 2019, 2020 Olivier Pirson
 * http://www.opimedia.be/
 * November 2, 2020
 */

/* round */
echo round(0.5), "\n";  /* 1 */


/* Euclidean division */
echo "intdiv %\n";
echo intdiv( 7,  3), ' ',  7 %  3, "\n";  /*  2  1    7 =  2 *  3 + 1 */
echo intdiv(-7,  3), ' ', -7 %  3, "\n";  /* -2 -1   -7 = -2 *  3 - 1 */
echo intdiv( 7, -3), ' ',  7 % -3, "\n";  /* -2  1    7 = -2 * -3 + 1 */
echo intdiv(-7, -3), ' ', -7 % -3, "\n";  /*  2 -1   -7 =  2 * -3 - 1 */

?>
