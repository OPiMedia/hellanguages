#!/usr/bin/env python3
# -*- coding: latin-1 -*-

"""
Little tests of round behaviour and Euclidean division
in Python 3.
https://docs.python.org/3/

Piece of hellanguages.
https://bitbucket.org/OPiMedia/hellanguages

:license: GPLv3 --- Copyright (C) 2019 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: January 19, 2019
"""

# round
print(round(0.5))  # 0 in Python 3 (1.0 in Python 2)

# Euclidean division
print('// %')
print( 7 //  3,  7 %  3)  #  2  1    7 =  2 *  3 + 1
print(-7 //  3, -7 %  3)  # -3  2   -7 = -3 *  3 + 2
print( 7 // -3,  7 % -3)  # -3 -2    7 = -3 * -3 - 2
print(-7 // -3, -7 % -3)  #  2 -1   -7 =  2 * -3 - 1
