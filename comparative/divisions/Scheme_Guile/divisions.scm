#!/usr/bin/env guile
!#

;;;
;;; Little tests of round behaviour and Euclidean division
;;; in Scheme (Guile).
;;; https://www.gnu.org/software/guile/manual/
;;;
;;; Piece of hellanguages.
;;; https://bitbucket.org/OPiMedia/hellanguages
;;;
;;; GPLv3 --- Copyright (C) 2019 Olivier Pirson
;;; http://www.opimedia.be/
;;; January 19, 2019
;;;

;;; round
(display (round 0.5))  ;; 0.0
(newline)


;;; Euclidean division
(display "quotient remainder modulo")
(newline)

;;;  7 =  2 *  3 + 1
(display (quotient 7 3))  ;;  2
(display " ")
(display (remainder 7 3))  ;; 1
(display " ")
(display (modulo 7 3))    ;;  1
(newline)

;;; -7 = -2 *  3 - 1
(display (quotient -7 3))  ;;   -2
(display " ")
(display (remainder -7 3))  ;;  -1
(display " ")
(display (modulo -7 3))    ;;   2
(newline)

;;;  7 = -2 * -3 + 1
(display (quotient 7 -3))  ;;  -2
(display " ")
(display (remainder 7 -3))  ;; 1
(display " ")
(display (modulo 7 -3))    ;;  -2
(newline)

;;; -7 =  2 * -3 - 1
(display (quotient -7 -3))  ;;   2
(display " ")
(display (remainder -7 -3))  ;;  -1
(display " ")
(display (modulo -7 -3))    ;;   -1
(newline)
