#!/usr/bin/env js
/*
 * Little tests of round behaviour and Euclidean division
 * in JavaScript (Node.js).
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript
 *
 * Piece of hellanguages.
 * https://bitbucket.org/OPiMedia/hellanguages
 *
 * GPLv3 --- Copyright (C) 2019 Olivier Pirson
 * http://www.opimedia.be/
 * January 19, 2019
 */

"use strict";

// round
console.log(Math.round(0.5));  // 1


// Euclidean division
console.log("Math.trunc(n/d) %");
console.log(Math.trunc( 7 /  3),  7 %  3);  //  2  1    7 =  2 *  3 + 1
console.log(Math.trunc(-7 /  3), -7 %  3);  // -2 -1   -7 = -2 *  3 - 1
console.log(Math.trunc( 7 / -3),  7 % -3);  // -2  1    7 = -2 * -3 + 1
console.log(Math.trunc(-7 / -3), -7 % -3);  //  2 -1   -7 =  2 * -3 - 1
