/**
 * Divisions.scala (January 19, 2019)
 *
 * Little tests of round behaviour and Euclidean division
 * in Scala.
 * https://docs.scala-lang.org/
 *
 * Piece of hellanguages.
 * https://bitbucket.org/OPiMedia/hellanguages
 *
 * GPLv3 --- Copyright (C) 2019 Olivier Pirson
 * http://www.opimedia.be/
 */

object Divisions {
  def main(args: Array[String]) {
    // round
    println(Math.round(0.5));  // 1


    // Euclidean division
    println("/ %");
    println(( 7 /  3) + " " + ( 7 %  3));  //  2  1    7 =  2 *  3 + 1
    println((-7 /  3) + " " + (-7 %  3));  // -2 -1   -7 = -2 *  3 - 1
    println(( 7 / -3) + " " + ( 7 % -3));  // -2  1    7 = -2 * -3 + 1
    println((-7 / -3) + " " + (-7 % -3));  //  2 -1   -7 =  2 * -3 - 1
  }
}
