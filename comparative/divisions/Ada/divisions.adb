--  divisions.abd (January 14, 2022)
--
--  Little tests of round behaviour and Euclidean division
--  in Ada.
--  https://docs.adacore.com/live/wave/arm12/html/arm12/arm12.html
--
--  Piece of hellanguages.
--  https://bitbucket.org/OPiMedia/hellanguages
--
--  GPLv3 --- Copyright (C) 2020, 2022 Olivier Pirson
--  http://www.opimedia.be/

with Ada.Text_IO; use Ada.Text_IO;


procedure Divisions is
begin
   --  round
   Put_Line (Float'Image (Float'Rounding (0.5)));  -- 1.00000E+00


   --  Euclidean division
   Put_Line ("/ rem mod");
   Put_Line (Integer'Image (  7  /   3)  & " " & Integer'Image (  7  rem   3)  & " " & Integer'Image (  7  mod   3));   --   2  1  1    7 =  2 *  3 + 1
   Put_Line (Integer'Image ((-7) /   3)  & " " & Integer'Image ((-7) rem   3)  & " " & Integer'Image ((-7) mod   3));   --  -2 -1  2   -7 = -2 *  3 - 1
   Put_Line (Integer'Image (  7  / (-3)) & " " & Integer'Image (  7  rem (-3)) & " " & Integer'Image (  7  mod (-3)));  --  -2  1 -2    7 = -2 * -3 + 1
   Put_Line (Integer'Image ((-7) / (-3)) & " " & Integer'Image ((-7) rem (-3)) & " " & Integer'Image ((-7) mod (-3)));  --   2 -1 -1   -7 =  2 * -3 - 1
end Divisions;
