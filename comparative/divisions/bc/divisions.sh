#!/bin/bash
#
# Little tests of round behaviour and Euclidean division
# in bc.
# https://en.wikipedia.org/wiki/Bc_(programming_language)
#
# Piece of hellanguages.
# https://bitbucket.org/OPiMedia/hellanguages
#
# GPLv3 --- Copyright (C) 2022 Olivier Pirson
# http://www.opimedia.be/
# February 19, 2022
#

# round
echo  # no round function in bc


# Euclidean division
echo '/ %'
echo $' 7 /  3\n  7 %  3' | bc | tr $'\n' ' '; echo  #  2  1    7 =  2 *  3 + 1
echo $'-7 /  3\n -7 %  3' | bc | tr $'\n' ' '; echo  # -2 -1   -7 = -2 *  3 - 1
echo $' 7 / -3\n  7 % -3' | bc | tr $'\n' ' '; echo  # -2  1    7 = -2 * -3 + 1
echo $'-7 / -3\n -7 % -3' | bc | tr $'\n' ' '; echo  #  2 -1   -7 =  2 * -3 - 1
