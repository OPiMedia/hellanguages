/* -*- coding: latin-1 -*- */
/*
 * divisions.c (November 2, 2020)
 *
 * Little tests of round behaviour and Euclidean division
 * in C.
 * https://en.cppreference.com/w/c
 *
 * Piece of hellanguages.
 * https://bitbucket.org/OPiMedia/hellanguages
 *
 * GPLv3 --- Copyright (C) 2019, 2020 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>


int
main() {
  /* round */
  printf("%f\n", round(0.5));  /* 1.000000 */


  /* Euclidean division */
  puts("/ %");
  printf("%i %i\n",  7 /  3,  7 %  3);  /*  2  1    7 =  2 *  3 + 1 */
  printf("%i %i\n", -7 /  3, -7 %  3);  /* -2 -1   -7 = -2 *  3 - 1 */
  printf("%i %i\n",  7 / -3,  7 % -3);  /* -2  1    7 = -2 * -3 + 1 */
  printf("%i %i\n", -7 / -3, -7 % -3);  /*  2 -1   -7 =  2 * -3 - 1 */


  return EXIT_SUCCESS;
}
