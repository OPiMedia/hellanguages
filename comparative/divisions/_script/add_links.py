#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Add links to programming languages in the divisions.html built.

Piece of hellanguages.
https://bitbucket.org/OPiMedia/hellanguages

:license: GPLv3 --- Copyright (C) 2022 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: February 19, 2022
"""

import sys


#
# Main
######
def main() -> None:
    """Main"""
    urls = {'Ada':         'http://www.opimedia.be/DS/Ada/',
            'bc':          'http://www.opimedia.be/DS/languages/#bc',
            'C':           'http://www.opimedia.be/DS/languages/#C',
            'C++':         'http://www.opimedia.be/DS/languages/#Cpp',
            'GMP':         'http://www.opimedia.be/DS/languages/#GMP',
            'Guile':       'http://www.opimedia.be/DS/languages/#Guile',
            'Java':        'http://www.opimedia.be/DS/languages/#Java',
            'JavaScript':  'http://www.opimedia.be/DS/webdev/liens.htm#JavaScript',  # noqa
            'NodeJS':     ('http://www.opimedia.be/DS/webdev/liens.htm#Node-js', 'Node.js'),  # noqa
            'PHP':         'http://www.opimedia.be/DS/webdev/liens.htm#PHP',
            'Python':      'http://www.opimedia.be/DS/Python/',
            'Scala':       'http://www.opimedia.be/DS/languages/#Scala',
            'Scheme':      'http://www.opimedia.be/DS/languages/#Scheme'}

    in_table = False
    for line in sys.stdin:
        line = line.rstrip()
        if in_table:
            for name, url in tuple(urls.items()):
                if (name in line) and (name in urls):
                    del urls[name]
                    if isinstance(url, tuple):
                        url, new_name = url
                    else:
                        new_name = name
                    line = line.replace(name,
                                        f'<a href="{url}">{new_name}</a>')
        elif '    <tbody>' in line:
            in_table = True
        print(line)


if __name__ == '__main__':
    main()
