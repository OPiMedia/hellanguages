#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Extract results for each language
build the TSV file.

Piece of hellanguages.
https://bitbucket.org/OPiMedia/hellanguages

:license: GPLv3 --- Copyright (C) 2019-2020, 2022 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: February, 2022
"""

from typing import List


#
# Function
##########
def read(directory: str) -> List[str]:
    """
    Read results contained in directory.
    """
    with open(directory + '/divisions.txt', encoding='latin-1') as fin:
        seq = ['|', fin.readline()[:-1]]

        for line in fin:
            seq.append('|')
            seq.extend((tuple(line.split()) + ('', ) * 3)[:3])

        return seq


#
# Main
######
def main() -> None:
    """
    Main
    """
    print('Language', '',
          'round(0.5)', '',
          'quotient', 'remainder', '', '',
          '7/3', 'remainder', '', '',
          '-7/3', 'remainder', '', '',
          '7/-3', 'remainder', '', '',
          '-7/-3', 'remainder', '',
          sep='\t')

    for name in ('Ada',
                 'bc',
                 'C', 'Cpp',
                 'GMP_C',
                 'Java',
                 'JavaScript_NodeJS',
                 'PHP',
                 'Python_2', 'Python_3',
                 'Scala',
                 'Scheme_Guile'):
        print(name.replace('_', ' ').replace('Cpp', 'C++'),
              *read(name),
              sep='\t')


if __name__ == '__main__':
    main()
