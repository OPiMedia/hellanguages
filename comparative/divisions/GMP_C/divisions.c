/* -*- coding: latin-1 -*- */
/*
 * divisions.c (January 19, 2019)
 *
 * Little tests of round behaviour and Euclidean division
 * with GMP library (in C).
 * https://gmplib.org/
 *
 * Piece of hellanguages.
 * https://bitbucket.org/OPiMedia/hellanguages
 *
 * GPLv3 --- Copyright (C) 2019 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <stdio.h>
#include <stdlib.h>

#include <gmp.h>


int
main() {
  /* round */
  puts("");


  /* Euclidean division */
  puts("mpz_div mpz_mod");

  mpz_t n;
  mpz_t d;
  mpz_t q;
  mpz_t r;

  mpz_init(q);
  mpz_init(r);

  /*  2  1    7  =  2 *  3 + 1 */
  mpz_init_set_si(n, 7);
  mpz_init_set_si(d, 3);
  mpz_div(q, n, d);
  mpz_mod(r, n, d);
  gmp_printf("%Zd %Zd\n", q, r);

  /* -3  2   -7  = -3 *  3 + 2 */
  mpz_set_si(n, -7);
  mpz_div(q, n, d);
  mpz_mod(r, n, d);
  gmp_printf("%Zd %Zd\n", q, r);

  /* -3   mpz_div() ignore the sign of the divisor */
  mpz_set_si(n, 7);
  mpz_set_si(d, -3);
  mpz_div(q, n, d);
  gmp_printf("%Zd\n", q);

  /*  2   mpz_div() ignore the sign of the divisor */
  mpz_set_si(n, -7);
  mpz_div(q, n, d);
  gmp_printf("%Zd\n", q);


  return EXIT_SUCCESS;
}
