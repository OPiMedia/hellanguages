/**
 * Divisions.java (January 19, 2019)
 *
 * Little tests of round behaviour and Euclidean division
 * in Java.
 * https://docs.oracle.com/javase/
 *
 * Piece of hellanguages.
 * https://bitbucket.org/OPiMedia/hellanguages
 *
 * GPLv3 --- Copyright (C) 2019 Olivier Pirson
 * http://www.opimedia.be/
 */

public class Divisions {
  public static void main(String args[]) {
    // round
    System.out.println(Math.round(0.5));  // 1


    // Euclidean division
    System.out.println("/ %");
    System.out.println(( 7 /  3) + " " + ( 7 %  3));  //  2  1    7 =  2 *  3 + 1
    System.out.println((-7 /  3) + " " + (-7 %  3));  // -2 -1   -7 = -2 *  3 - 1
    System.out.println(( 7 / -3) + " " + ( 7 % -3));  // -2  1    7 = -2 * -3 + 1
    System.out.println((-7 / -3) + " " + (-7 % -3));  //  2 -1   -7 =  2 * -3 - 1
  }
}
