/* -*- coding: latin-1 -*- */
/*
 * divisions.cpp (November 2, 2020)
 *
 * Little tests of round behaviour and Euclidean division
 * in C++.
 * https://en.cppreference.com/w/cpp
 *
 * Piece of hellanguages.
 * https://bitbucket.org/OPiMedia/hellanguages
 *
 * GPLv3 --- Copyright (C) 2019, 2020 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <cmath>
#include <cstdlib>

#include <iostream>


int
main() {
  // round
  std::cout << round(0.5) << std::endl;  // 1


  // Euclidean division
  std::cout << "/ %" << std::endl;
  std::cout <<  7 /  3 << ' '<<  7 %  3 << std::endl;  //  2  1    7 =  2 *  3 + 1
  std::cout << -7 /  3 << ' '<< -7 %  3 << std::endl;  // -2 -1   -7 = -2 *  3 - 1
  std::cout <<  7 / -3 << ' '<<  7 % -3 << std::endl;  // -2  1    7 = -2 * -3 + 1
  std::cout << -7 / -3 << ' '<< -7 % -3 << std::endl;  //  2 -1   -7 =  2 * -3 - 1


  return EXIT_SUCCESS;
}
