# comparative shell / if ... ; then

Try of missing `;` in `if ... ; then` in sh, Bash, Ksh83 (emulated via Zsh), Ksh93 and Zsh.

Question asked on *Unix & Linux Stack Exchange*:
*if var='value' then in Zsh: Is it really a valid syntax without semicolon?*
https://unix.stackexchange.com/questions/748701/if-var-value-then-in-zsh-is-it-really-a-valid-syntax-without-semicolon



## Link
* [References about sh, ksh, bash and zsh](http://www.opimedia.be/DS/languages/#sh)
