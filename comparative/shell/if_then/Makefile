# Building result files and static analysis --- June 19, 2023

SRCS_WITHOUT_ZSH = if_then.sh if_then.bash if_then.ksh83 if_then.ksh93
SRCS = $(SRCS_WITHOUT_ZSH) if_then.zsh

TXTS = $(foreach SRC,$(SRCS),$(SRC).txt)


.SUFFIXES:

SHELLCHECK      = shellcheck  # https://www.shellcheck.net/
SHELLCHECKFLAGS =


ECHO  = echo
HEAD  = head
RM    = rm -f
SHELL = sh
TEE   = tee



###
# #
###
.PHONY:	all

all:	$(TXTS)

if_then.ksh83.txt:	if_then.ksh83
	-zsh $^ > $@ 2>&1



###################
# Static analysis #
###################
.PHONY:	shellcheck lint lintlog

lint:	shellcheck

lintlog:
	@$(ECHO) 'Lint ('`date`') of if_then.sh' | $(TEE) lint.log
	@$(ECHO) | $(TEE) -a lint.log
	@$(ECHO) ===== `$(SHELLCHECK) $(SHELLCHECKFLAGS) --version | $(HEAD) -n 1` ===== | $(TEE) -a lint.log
	-$(SHELLCHECK) -s ksh $(SHELLCHECKFLAGS) $(SRCS_WITHOUT_ZSH) | $(TEE) -a lint.log


shellcheck:
	@$(ECHO) ===== `$(SHELLCHECK) $(SHELLCHECKFLAGS) --version | $(HEAD) -n 1` =====
	-$(SHELLCHECK) -s ksh $(SHELLCHECKFLAGS) $(SRCS_WITHOUT_ZSH)
	@$(ECHO)



########
# Rule #
########
%.txt:	%
	-./$^ > $@ 2>&1



#########
# Clean #
#########
.PHONY:	clean cleanLint distclean overclean

clean:	cleanResult

cleanLint:
	$(RM) lint.log

distclean:	clean

overclean:	distclean cleanLint
