#!/bin/bash

echo "Bash $BASH_VERSION"


echo 'Correct syntax'
if true; then
    echo 'then'
fi
if var='value'; then
    echo 'then'
fi


echo
echo 'Incorrect syntax'
if var='value' then
   echo 'then'
fi
