#!/bin/zsh

echo "Zsh $ZSH_VERSION"


echo 'Correct syntax'
if true; then
    echo 'then'
fi
if var='value'; then
    echo 'then'
fi


echo
echo 'Correct syntax in Zsh?'
if var='value' then
   echo 'then'
fi


echo
echo 'Incorrect syntax'
if true then
   echo 'then'
fi
