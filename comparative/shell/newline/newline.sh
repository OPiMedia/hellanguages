#!/bin/sh

echo 'sh'



NL='\n'

LINE="one\ntwo\nthree\n"

MULTILINE='one
two
three
'


run() {
    echo "========== With $* =========="
    echo '----- NL -----'
    "$@" "$NL"
    echo '----- LINE -----'
    "$@" "$LINE"
    echo '----- MULTLINE -----'
    "$@" "$MULTILINE"
}


run echo -n
run printf '%s'
