#!/bin/zsh

echo "Zsh $ZSH_VERSION"



#
# Arrays
#
echo '# Example of use of arrays in Zsh: https://hyperpolyglot.org/unix-shells#arrays'

typeset -a ARRAY

ARRAY+=(first)
ARRAY+=('second with some spaces')
ARRAY+=(third)

echo "Size: ${#ARRAY[@]}"

for I in $(seq ${#ARRAY}); do  # from 1 to size
    echo "  $I: ${ARRAY[$I]}"
done

for VALUE in "${ARRAY[@]}"; do
    echo "  * $VALUE"
done


ARRAY[2]=()  # remove element and reindexing

echo "Size: ${#ARRAY[*]}"

for I in $(seq ${#ARRAY}); do
    echo "  $I: ${ARRAY[$I]}"
done



echo

#
# Associative arrays
#
echo '# Example of use of associative array in Zsh: https://hyperpolyglot.org/unix-shells#associative-arrays'

typeset -A ASSOC

ASSOC[one]=first
ASSOC[two]='second with some spaces'
ASSOC['three with spaces']=third

echo "Size: ${#ASSOC[@]}"

for KEY in ${(@k)ASSOC}; do
    echo "  $KEY: ${ASSOC[$KEY]}"
done

for VALUE in "${ASSOC[@]}"; do
    echo "  * $VALUE"
done

for KEY VALUE in ${(@kv)ASSOC}; do
    echo "  $KEY: $VALUE"
done


unset "ASSOC[two]"

echo "Size: ${#ASSOC[@]}"

for KEY VALUE in ${(@kv)ASSOC}; do
    echo "  $KEY: $VALUE"
done
