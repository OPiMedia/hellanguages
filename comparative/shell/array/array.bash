#!/bin/bash

echo "Bash $BASH_VERSION"



#
# Arrays
#
echo '# Example of use of arrays in Bash: https://hyperpolyglot.org/unix-shells#arrays'

declare -a ARRAY

ARRAY+=(first)
ARRAY+=('second with some spaces')
ARRAY+=(third)

echo "Size: ${#ARRAY[@]}"

for I in "${!ARRAY[@]}"; do  # from 0 to size-1
    echo "  $I: ${ARRAY[$I]}"
done

for VALUE in "${ARRAY[@]}"; do
    echo "  * $VALUE"
done


unset "ARRAY[1]"  # remove element but NOT reindexing

echo "Size: ${#ARRAY[*]}"

for I in "${!ARRAY[@]}"; do
    echo "  $I: ${ARRAY[$I]}"
done



echo

#
# Associative arrays
#
echo '# Example of use of associative array in Bash: https://hyperpolyglot.org/unix-shells#associative-arrays'

declare -A ASSOC

ASSOC[one]=first
ASSOC[two]='second with some spaces'
ASSOC['three with spaces']=third

echo "Size: ${#ASSOC[@]}"

for KEY in "${!ASSOC[@]}"; do
    echo "  $KEY: ${ASSOC[$KEY]}"
done

for VALUE in "${ASSOC[@]}"; do
    echo "  * $VALUE"
done


unset "ASSOC[two]"

echo "Size: ${#ASSOC[@]}"

for KEY in "${!ASSOC[@]}"; do
    echo "  $KEY: ${ASSOC[$KEY]}"
done
