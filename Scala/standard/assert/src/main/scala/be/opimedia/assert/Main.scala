/**
  * assert/Main.java (March 16, 2019)
  *
  * Simple program to test behavior
  * of assert(), assume(), ensuring() and require().
  *
  * See also "Scala Standard Library - scala.Predef"
  * [[https://www.scala-lang.org/api/current/scala/Predef$.html]]
  *
  * and "Scala preconditions (assert, assume, require, ensuring)"
  * [[https://maxondev.com/scala-preconditions-assert-assume-require-ensuring/]]
  *
  * Piece of hellanguages.
  * [[https://bitbucket.org/OPiMedia/hellanguages]]
  *
  * GPLv3 --- Copyright (C) 2018, 2019 Olivier Pirson
  * [[http://www.opimedia.be/]]
  */
package be.opimedia.assert


object MainAssert extends App {
  println("Before assert()")
  System.out.flush

  /*
   * assert() is enabled by default.
   * Disabled if -Xdisable-assertions in scalac options
   * The JVM running parameters do not change anything.
   */
  assert(false)

  println("After assert()")
  System.out.flush
}



object MainAssume extends App {
  println("Before assume()")
  System.out.flush

  /*
   * assume() is enabled by default.
   * Disabled if -Xdisable-assertions in scalac options
   * The JVM running parameters do not change anything.
   *
   * Same behavior than assert().
   */
  assume(false)

  println("After assume()")
  System.out.flush
}



object MainEnsuring extends App {
  println("Before ensuring()")
  System.out.flush

  /*
   * ensuring() is normally *always* enabled
   * but it doesn't work for me!
   *
   * Tested with Scala 2.12.7, sbt 1.2.8, Java 1.8.0_202 GraalVM EE 1.0.0-rc13.
   */
  def foo: Int = { 42 } ensuring(_ == 666)

  println("After ensuring()")
  System.out.flush
}



object MainRequire extends App {
  println("Before require()")
  System.out.flush

  /*
   * require() is *always* enabled!
   */
  require(false)

  println("After require()")
  System.out.flush
}
