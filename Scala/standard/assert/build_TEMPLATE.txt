name         := "assert"
organization := "be.opimedia"
version      := "01.00.00"



cancelable in Global := true  // don't quit SBT when CTRL-C


scalaVersion := "2.12.7"
// scalacOptions := Seq("-Xdisable-assertions")

mainClass in (Compile, run) := Some("be.opimedia.assert.MainAssert")



// Build complete JAR file with project and all libraries included Scala
mainClass in assembly := Some("be.opimedia.assert.MainAssert")
assemblyJarName in assembly := "../../assert__enabled.jar"
