addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.14.8")  // to build JAR of the complete project
addSbtPlugin("org.scalastyle" %% "scalastyle-sbt-plugin" % "1.0.0")  // Scala style checker
