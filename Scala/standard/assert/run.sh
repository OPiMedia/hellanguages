#!/bin/sh


if [ -z "$1" ]
then
    MAIN=MainAssert
else
    MAIN=$1
fi

echo DISABLED
echo ===== java -cp assert__disabled.jar be.opimedia.assert.$MAIN =====
java -cp assert__disabled.jar be.opimedia.assert.$MAIN

echo
echo ===== java -da -cp assert__disabled.jar be.opimedia.assert.$MAIN =====
java -da -cp assert__disabled.jar be.opimedia.assert.$MAIN

echo
echo ===== java -ea -cp assert__disabled.jar be.opimedia.assert.$MAIN =====
java -ea -cp assert__disabled.jar be.opimedia.assert.$MAIN



echo
echo
echo
echo ENABLED
echo ===== java -cp assert__enabled.jar be.opimedia.assert.$MAIN =====
java -cp assert__enabled.jar be.opimedia.assert.$MAIN

echo
echo ===== java -da -cp assert__enabled.jar be.opimedia.assert.$MAIN =====
java -da -cp assert__enabled.jar be.opimedia.assert.$MAIN

echo
echo ===== java -ea -cp assert__enabled.jar be.opimedia.assert.$MAIN =====
java -ea -cp assert__enabled.jar be.opimedia.assert.$MAIN
