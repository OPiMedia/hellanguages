/**
  *
  *
  * GPLv3 --- Copyright (C) 2019 Olivier Pirson
  * http://www.opimedia.be/
  */
package be.opimedia.template

import org.scalatest.{ BeforeAndAfterAll, FlatSpecLike, Matchers }
import akka.actor.{ Actor, Props, ActorSystem }
import akka.testkit.{ ImplicitSender, TestKit, TestActorRef, TestProbe }
import scala.concurrent.duration._

class MainSpec(_system: ActorSystem) extends TestKit(_system)
    with Matchers with FlatSpecLike with BeforeAndAfterAll {
}
