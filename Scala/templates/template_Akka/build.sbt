name         := "template_Akka"
organization := "be.opimedia"
version      := "00.00.00"



fork := true  // run tasks in forked JVM
cancelable in Global := true  // don't quit SBT when CTRL-C



scalaVersion := "2.12.7"

// https://docs.scala-lang.org/overviews/compiler-options/
scalacOptions ++= Seq("-J-Xmx3G")
scalacOptions ++= Seq(
  "-deprecation",
  "-explaintypes",
  "-feature",
  "-unchecked",

  "-Xlint:constant",
  "-Xlint:doc-detached",
  "-Xlint:unused",

  "-Ywarn-dead-code",
  "-Ywarn-nullary-override",
  "-Ywarn-nullary-unit",
  "-Ywarn-unused:_",

  "-Ywarn-unused:params",
  "-Ywarn-unused:patvars",
  "-Ywarn-unused:privates",
  "-Ywarn-value-discard"
)

// Options to disable during development and debug
// scalacOptions ++= Seq("-g:none", "-Xdisable-assertions", "-opt:unreachable-code", "-opt:simplify-jumps", "-opt:compact-locals", "-opt:copy-propagation", "-opt:redundant-casts", "-opt:box-unbox", "-opt:nullness-tracking", "-opt:l:inline", "-opt-inline-from")

maxErrors := 5



libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.14.0" % "test"

libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.8" % "test"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.8" % "test"

lazy val akkaVersion = "2.5.23"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % akkaVersion,
  "com.typesafe.akka" %% "akka-testkit" % akkaVersion
)



// Build complete JAR file with project (without test) and all libraries included Scala
assemblyJarName in assembly := "../../template_Akka.jar"
test in assembly := {}



// Building HTML doc
target in Compile in doc := baseDirectory.value / "docs/html"
