# hellanguages
Some tests/examples in various languages.

- [Bash](https://bitbucket.org/OPiMedia/hellanguages/src/master/Bash/)

    - [reads](https://bitbucket.org/OPiMedia/hellanguages/src/master/Bash/standard/reads/) multiple files line by line

- [C](https://bitbucket.org/OPiMedia/hellanguages/src/master/C/)

    - [C template](https://bitbucket.org/OPiMedia/hellanguages/src/master/C/template/)
    - benchmark:
      [iteration](https://bitbucket.org/OPiMedia/hellanguages/src/master/C/benchmark/iteration/)
    - integer:
      [floor_sqrt_c](https://bitbucket.org/OPiMedia/hellanguages/src/master/C/integer/floor_sqrt_c/)
    - standard:
      [cmps](https://bitbucket.org/OPiMedia/hellanguages/src/master/C/standard/cmps/),
      [mul2_div2](https://bitbucket.org/OPiMedia/hellanguages/src/master/C/standard/mul2_div2/),
      [passing_multi_array](https://bitbucket.org/OPiMedia/hellanguages/src/master/C/standard/passing_multi_array/)
    - [External useful C's links](http://www.opimedia.be/DS/languages/#C)

- [C++](https://bitbucket.org/OPiMedia/hellanguages/src/master/Cpp/)

    - [C++ template](https://bitbucket.org/OPiMedia/hellanguages/src/master/Cpp/template/)
    - classes:
      [constructors](https://bitbucket.org/OPiMedia/hellanguages/src/master/Cpp/class/constructors/),
      [private_var_cracked](https://bitbucket.org/OPiMedia/hellanguages/src/master/Cpp/class/private_var_cracked/),
      [static_vs_virtual](https://bitbucket.org/OPiMedia/hellanguages/src/master/Cpp/class/static_vs_virtual/)
    - integer:
      [floor_sqrt_cpp](https://bitbucket.org/OPiMedia/hellanguages/src/master/Cpp/integer/floor_sqrt_cpp/)
    - sound:
      [MIDI](https://bitbucket.org/OPiMedia/hellanguages/src/master/Cpp/sound/MIDI/) /
      [simple_rtmidi](https://bitbucket.org/OPiMedia/hellanguages/src/master/Cpp/sound/MIDI/RtMidi/simple_rtmidi/)
    - standard:
      [all_sizes](https://bitbucket.org/OPiMedia/hellanguages/src/master/Cpp/standard/all_sizes/),
      [un_signed](https://bitbucket.org/OPiMedia/hellanguages/src/master/Cpp/standard/un_signed/),
      [virtual](https://bitbucket.org/OPiMedia/hellanguages/src/master/Cpp/standard/virtual/)
    - thread:
      [simple_thread_cpp11](https://bitbucket.org/OPiMedia/hellanguages/src/master/Cpp/thread/simple_thread_cpp11/)
    - [External useful C++'s links](http://www.opimedia.be/DS/languages/#Cpp)

- [Java](https://bitbucket.org/OPiMedia/hellanguages/src/master/Java/)

    - compile:
      [TestGCJ](https://bitbucket.org/OPiMedia/hellanguages/src/master/Java/compile/TestGCJ/) - GNU Compiler for Java
    - sound:
      MIDI / [SimpleMidiSynthesis](https://bitbucket.org/OPiMedia/hellanguages/src/master/Java/sound/MIDI/SimpleMidiSynthesis/)
    - standard:
      [imports](https://bitbucket.org/OPiMedia/hellanguages/src/master/Java/standard/imports/),
      [Java Assert](https://bitbucket.org/OPiMedia/hellanguages/src/master/Java/standard/Assert/),
      [TestVarargs](https://bitbucket.org/OPiMedia/hellanguages/src/master/Java/standard/TestVarargs/),
      [two_mains](https://bitbucket.org/OPiMedia/hellanguages/src/master/Java/standard/two_mains/)
    - [External useful Java's links](http://www.opimedia.be/DS/languages/#Java)

- [Processing](https://bitbucket.org/OPiMedia/hellanguages/src/master/Processing/)
    - [empty](https://bitbucket.org/OPiMedia/hellanguages/src/master/Processing/empty/):
      empty Processing project, to see the minimal Java class automatically created.
    - [fail_with_same_class](https://bitbucket.org/OPiMedia/hellanguages/src/master/Processing/fail_with_same_class/):
      example to see that to define a class with the same name than the project is an error.

- [Python](https://bitbucket.org/OPiMedia/hellanguages/src/master/Python/)

    - [Python template](https://bitbucket.org/OPiMedia/hellanguages/src/master/Python/template/)
    - standard:
      [multiple inheritance](https://bitbucket.org/OPiMedia/hellanguages/src/master/Python/standard/multiple_inheritance/)
    - [External useful Python's links](http://www.opimedia.be/DS/Python/)
    - External [python3-example-pkg](https://python3-example-pkg.readthedocs.io/) – small example of Python 3 package

- [Scala](https://bitbucket.org/OPiMedia/hellanguages/src/master/Scala/)

    - [Scala templates](https://bitbucket.org/OPiMedia/hellanguages/src/master/Scala/templates/)
    - standard:
      [Scala assert](https://bitbucket.org/OPiMedia/hellanguages/src/master/Scala/standard/assert/)
    - [External useful Scala's links](http://www.opimedia.be/DS/languages/#Scala)

- comparative between languages

    - [divisions](https://bitbucket.org/OPiMedia/hellanguages/src/master/comparative/divisions/):
      tests of round of 0.5 behaviour and Euclidean division (see [divisions results](http://www.opimedia.be/DS/languages/comparative/divisions/divisions.html))
    - shell

        - [array](https://bitbucket.org/OPiMedia/hellanguages/src/master/comparative/shell/array/):
          examples of use of arrays and associative arrays in Bash, Ksh and Zsh
        - [if_then](https://bitbucket.org/OPiMedia/hellanguages/src/master/comparative/shell/if_then/):
          try of missing `;` in `if ... ; then` in sh, Bash, Ksh and Zsh
        - [newline](https://bitbucket.org/OPiMedia/hellanguages/src/master/comparative/shell/newline/):
          displaying of newlines by `echo` and `printf`



### Other useful links
- <http://www.opimedia.be/DS/languages/>
- <http://www.opimedia.be/DS/Ada/>
- <http://www.opimedia.be/DS/languages/Processing/>
- <http://www.opimedia.be/DS/Python/>
- <http://www.opimedia.be/DS/languages/tabs-vs-spaces/>



## Author: 🌳 Olivier Pirson — OPi ![OPi][OPi] 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬
🌐 Website: <http://www.opimedia.be/>

💾 Bitbucket: <https://bitbucket.org/OPiMedia/workspace/repositories>

- 📧 <olivier.pirson.opi@gmail.com>
- 🧑‍🤝‍🧑 Mastodon: <https://mamot.fr/@OPiMedia> — Twitter: <https://twitter.com/OPirson>
- 👨‍💻 LinkedIn: https://www.linkedin.com/in/olivierpirson/ — CV: http://www.opimedia.be/CV/English.html
- other profiles: <http://www.opimedia.be/about/>

[OPi]: http://www.opimedia.be/_png/OPi.png



## License: [GPLv3](https://www.gnu.org/licenses/gpl-3.0.html) ![GPLv3](https://www.gnu.org/graphics/gplv3-88x31.png)
Copyright (C) 2014 – 2020, 2022 – 2023 Olivier Pirson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.



![hellanguages](https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2020/Apr/19/2749790018-0-hellanguages-logo_avatar.png)
