/* -*- coding: latin-1 -*- */
/*
 * constructors.cpp (December 20, 2014)
 *
 * Example of different constructors and initializations.
 *
 *
 * Piece of hellanguages.
 * https://bitbucket.org/OPiMedia/hellanguages
 *
 * GPLv3 --- Copyright (C) 2014 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <cstdlib>

#include <iostream>



class ClassExample {
 public:
  ClassExample() {
    std::cout << "ClassExample()" << std::endl;
  }


  ClassExample(const std::initializer_list<int>&) {
    std::cout << "ClassExample(const std::initializer_list<int>&)" << std::endl;
  }
};



int
main () {
  std::cout << "a: ";
  ClassExample a;  // ClassExample()
  std::cout << std::endl;


  std::cout << "b1(): ";
  ClassExample b1();  // declare function b1()
  std::cout << std::endl;

  std::cout << "b2{}: ";
  ClassExample b2{};  // ClassExample()
  std::cout << std::endl;

  std::cout << "b3 = {}: ";
  ClassExample b3 = {};  // ClassExample()
  std::cout << std::endl;


  std::cout << "c1({1, 2, 3}): ";
  ClassExample c1({1, 2, 3});  // ClassExample(const initializer_list<int>&)
  std::cout << std::endl;

  std::cout << "c2{1, 2, 3}: ";
  ClassExample c2{1, 2, 3};  // ClassExample(const initializer_list<int>&)
  std::cout << std::endl;

  std::cout << "c3 = {1, 2, 3}: ";
  ClassExample c3 = {1, 2, 3};  // ClassExample(const initializer_list<int>&)
  std::cout << std::endl;

  return EXIT_SUCCESS;
}
