/* -*- coding: latin-1 -*- */
/*
 * static_vs_virtual.cpp (October 30, 2016)
 *
 * Example of difference
 * between static dispatch and dynamic dispatch (virtual method).
 *
 *
 * Piece of hellanguages.
 * https://bitbucket.org/OPiMedia/hellanguages
 *
 * GPLv3 --- Copyright (C) 2016 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <iostream>



class A {
public:
  void
  foo_static() {
    std::cout << "foo_static() of A" << std::endl;
  }


  virtual
  void
  foo_virtual() {
    std::cout << "foo_virtual() of A" << std::endl;
  }
};


class B : public A {
public:
  void
  foo_static() {
    std::cout << "foo_static() of B" << std::endl;
  }


  virtual
  void
  foo_virtual() {
    std::cout << "foo_virtual() of B" << std::endl;
  }
};



int
main () {
  A a;
  B b;

  A *ptr_to_A = &b;


  std::cout << "Static dispatch:" << std::endl;
  a.foo_static();  // foo_static() of A
  b.foo_static();  // foo_static() of B

  ptr_to_A->foo_static();  // foo_static() of A


  std::cout << std::endl
            << "Dynamic dispatch:" << std::endl;
  a.foo_virtual();  // foo_virtual() of A
  b.foo_virtual();  // foo_virtual() of B

  ptr_to_A->foo_virtual();  // foo_virtual() of B

  return EXIT_SUCCESS;
}
