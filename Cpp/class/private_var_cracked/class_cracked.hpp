/* -*- coding: latin-1 -*- */
/*
 * private_var_cracked/class_cracked.hpp (December 3, 2014)
 *
 * Piece of hellanguages.
 * https://bitbucket.org/OPiMedia/hellanguages
 *
 * GPLv3 --- Copyright (C) 2014 Olivier Pirson
 * http://www.opimedia.be/
 */

#ifndef _INCLUDED_CLASS
#define _INCLUDED_CLASS


class ClassWithPrivateVar {
public:
  ClassWithPrivateVar(int value);


  /* Added method to access to the private variable */
  int get_private_var() const {
    return private_var;
  }


private:
  int private_var;
};


#endif
