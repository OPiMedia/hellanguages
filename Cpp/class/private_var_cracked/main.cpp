/* -*- coding: latin-1 -*- */
/*
 * private_var_cracked
 *
 * Example added method in include file to "crack" private variable.
 *
 *
 * private_var_cracked/main.cpp (December 3, 2014)
 *
 * Piece of hellanguages.
 * https://bitbucket.org/OPiMedia/hellanguages
 *
 * GPLv3 --- Copyright (C) 2014 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <iostream>

// #include "class.hpp"  // normal header
#include "class_cracked.hpp"  // modified header



int
main() {
  ClassWithPrivateVar o(42);

  // int value(o.private_var);  // error: 'int ClassWithPrivateVar::private_var' is private
  int value(o.get_private_var());

  std::cout << "Value of the private variable: " << value << std::endl;

  return EXIT_SUCCESS;
}
