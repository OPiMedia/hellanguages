/* -*- coding: latin-1 -*- */
/*
 * private_var_cracked/class.cpp (November 28, 2014)
 *
 * Piece of hellanguages.
 * https://bitbucket.org/OPiMedia/hellanguages
 *
 * GPLv3 --- Copyright (C) 2014 Olivier Pirson
 * http://www.opimedia.be/
 */

#include "class.hpp"



ClassWithPrivateVar::ClassWithPrivateVar(int value): private_var(value) {}
