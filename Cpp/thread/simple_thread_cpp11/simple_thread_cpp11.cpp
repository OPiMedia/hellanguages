/* -*- coding: latin-1 -*- */
/*
 * simple_thread_cpp11.cpp (November 25, 2014)
 *
 * Simple thread C++11 example
 *
 *
 * Piece of hellanguages.
 * https://bitbucket.org/OPiMedia/hellanguages
 *
 * GPLv3 --- Copyright (C) 2014 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <iostream>
#include <mutex>
#include <thread>



std::mutex global_mutex;



void
thread_function() {
  global_mutex.lock();

  std::cout << "Thread 1 start" << std::endl;

  for (int i(0); i < 20; ++i) {
    std::cout << "Thread 1" << std::endl;
  }

  std::cout << "Thread 1 end" << std::endl;

  global_mutex.unlock();
}



int
main() {
  std::thread thread(thread_function);

  global_mutex.lock();

  for (int i(0); i < 20; ++i) {
    std::cout << "Thread principal" << std::endl;
  }

  global_mutex.unlock();

  thread.join();

  return EXIT_SUCCESS;
}
