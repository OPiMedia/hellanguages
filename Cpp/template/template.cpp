/* -*- coding: latin-1 -*- */
/*
 * template.cpp (July 4, 2020)
 *
 * GPLv3 --- Copyright (C) 2020 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <cstdlib>

#include <iostream>


int
main(int argc, const char* const argv[]) {
  std::cout << argc << ' ' << argv[0] << std::endl;

  return EXIT_SUCCESS;
}
