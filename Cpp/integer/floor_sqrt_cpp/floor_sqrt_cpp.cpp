/* -*- coding: latin-1 -*- */
/**
 * floor_sqrt_cpp.cpp (April 19, 2020)
 *
 * Function to return the *exact* square root of n rounded to the below.
 * Use the sqrt() double function from the math library, and correct the rounding errors.
 *
 * Normally it is not necessary for numbers on 8, 16 or 32 bits.
 * Only bigger numbers are not always an exact representation in double.
 *
 * C version here:
 * https://bitbucket.org/OPiMedia/hellanguages/src/master/C/integer/floor_sqrt_c/
 *
 * GPLv3 --- Copyright (C) 2017, 2019 - 2020 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <cassert>
#include <cmath>
#include <iostream>
#include <cstdint>
#include <cstdbool>
#include <cstdlib>


uint8_t
floor_sqrt8(uint8_t n) {
  if (n >= 225) {  // >= (2^(8/2) - 1)^2 = 225
    return 15;  // = 2^(8/2) - 1
  }
  else {
    uint8_t sqrt_n = static_cast<uint8_t>(floor(sqrt(static_cast<double>(n))));

    /* Correct possible rounding error due to floating point computation */
    while (sqrt_n*sqrt_n <= n) {  /* get the first value too big */
      ++sqrt_n;
    }
    do  {  /* get the correct value */
      --sqrt_n;
    } while (sqrt_n*sqrt_n > n);

    return sqrt_n;
  }
}


uint16_t
floor_sqrt16(uint16_t n) {
  if (n >= 65025) {  // >= (2^(16/2) - 1)^2 = 65025
    return 255;  // = 2^(16/2) - 1
  }
  else {
    uint16_t sqrt_n = static_cast<uint16_t>(floor(sqrt(static_cast<double>(n))));

    /* Correct possible rounding error due to floating point computation */
    while (sqrt_n*sqrt_n <= n) {  /* get the first value too big */
      ++sqrt_n;
    }
    do  {  /* get the correct value */
      --sqrt_n;
    } while (sqrt_n*sqrt_n > n);

    return sqrt_n;
  }
}


uint32_t
floor_sqrt32(uint32_t n) {
  if (n >= 4294836225u) {  // >= (2^(32/2) - 1)^2 = 4294836225
    return 65535;  // = 2^(32/2) - 1
  }
  else {
    uint32_t sqrt_n = static_cast<uint32_t>(floor(sqrt(static_cast<double>(n))));

    /* Correct possible rounding error due to floating point computation */
    while (sqrt_n*sqrt_n <= n) {  /* get the first value too big */
      ++sqrt_n;
    }
    do  {  /* get the correct value */
      --sqrt_n;
    } while (sqrt_n*sqrt_n > n);

    return sqrt_n;
  }
}


uint64_t
floor_sqrt64(uint64_t n) {
  if (n >= 18446744065119617025ul) {  // >= (2^(64/2) - 1)^2 = 18446744065119617025
    return 4294967295;  // = 2^(64/2) - 1
  }
  else {
    uint64_t sqrt_n = static_cast<uint64_t>(floor(sqrt(static_cast<double>(n))));
    // maybe use long double instead?

    /* Correct possible rounding error due to floating point computation */
    while (sqrt_n*sqrt_n <= n) {  /* get the first value too big */
      ++sqrt_n;
    }
    do  {  /* get the correct value */
      --sqrt_n;
    } while (sqrt_n*sqrt_n > n);

    return sqrt_n;
  }
}



int
main() {
  /* Print first 30 results */
  for (uint8_t n = 0; n < 30; ++n) {
    const uint16_t sqrt_n = floor_sqrt8(n);

    std::cout << static_cast<unsigned int>(n)
              << '\t' << sqrt_n*sqrt_n
              << '\t' << sqrt_n
              << std::endl;
  }


  /* Some isolated tests */
  assert(floor_sqrt64( 0) == 0);
  assert(floor_sqrt64( 1) == 1);
  assert(floor_sqrt64( 2) == 1);
  assert(floor_sqrt64( 3) == 1);
  assert(floor_sqrt64( 4) == 2);
  assert(floor_sqrt64( 5) == 2);
  assert(floor_sqrt64( 6) == 2);
  assert(floor_sqrt64( 7) == 2);
  assert(floor_sqrt64( 8) == 2);
  assert(floor_sqrt64( 9) == 3);
  assert(floor_sqrt64(10) == 3);

  assert(floor_sqrt64(15) == 3);
  assert(floor_sqrt64(16) == 4);
  assert(floor_sqrt64(17) == 4);

  assert(floor_sqrt64(24) == 4);
  assert(floor_sqrt64(25) == 5);
  assert(floor_sqrt64(26) == 5);

  {
    const uint64_t sqrt_n = 1000;

    assert(floor_sqrt64(sqrt_n*sqrt_n - 1) == sqrt_n - 1);
    assert(floor_sqrt64(sqrt_n*sqrt_n    ) == sqrt_n);
    assert(floor_sqrt64(sqrt_n*sqrt_n + 1) == sqrt_n);
  }

  {
    const uint64_t sqrt_n = 1000000000ul;

    assert(floor_sqrt64(sqrt_n*sqrt_n - 1) == sqrt_n - 1);
    assert(floor_sqrt64(sqrt_n*sqrt_n    ) == sqrt_n);
    assert(floor_sqrt64(sqrt_n*sqrt_n + 1) == sqrt_n);
  }


  for (unsigned int i = 2; i <= 63;  i += 2) {
    assert(floor_sqrt64(((static_cast<uint64_t>(1)) << i) - 1)
           == ((static_cast<uint64_t>(1)) << (i/2)) - 1);
    assert(floor_sqrt64( (static_cast<uint64_t>(1)) << i)
           ==  (static_cast<uint64_t>(1)) << (i/2));
    assert(floor_sqrt64(((static_cast<uint64_t>(1)) << i) + 1)
           == ((static_cast<uint64_t>(1)) << (i/2)));
  }


  /* Check */
  std::cout << "Check on 8 bits...";
  for (uint64_t n = 0; n < ((static_cast<uint64_t>(1)) << 8); ++n) {
    const uint64_t sqrt_n = floor_sqrt8(n);

    assert(sqrt_n == (sqrt_n % 256));

    assert(sqrt_n*sqrt_n <= n);
    assert(n < (sqrt_n + 1)*(sqrt_n + 1));

    assert(sqrt_n == floor_sqrt16(n));
    assert(sqrt_n == floor_sqrt32(n));
    assert(sqrt_n == floor_sqrt64(n));
  }
  std::cout << std::endl;


  std::cout << "Check on 16 bits...";
  for (uint64_t n = 0; n < ((static_cast<uint64_t>(1)) << 16); ++n) {
    const uint64_t sqrt_n = floor_sqrt16(n);

    assert(sqrt_n == (sqrt_n % 65536));

    assert(sqrt_n*sqrt_n <= n);
    assert(n < (sqrt_n + 1)*(sqrt_n + 1));

    assert(sqrt_n == floor_sqrt32(n));
    assert(sqrt_n == floor_sqrt64(n));
  }
  std::cout << std::endl;


  std::cout << "Check on 32 bits...";

  bool all_correct = true;

  for (uint64_t n = 0; n < ((static_cast<uint64_t>(1)) << 32); ++n) {
    const uint64_t sqrt_n = floor_sqrt32(n);

    if (all_correct) {
      const uint64_t approx_sqrt_n = floor(sqrt(static_cast<double>(n)));

      if (sqrt_n != approx_sqrt_n) {
        all_correct = false;
        std::cout << std::endl << std::endl
                  << "[!!! first error on exact sqrt(" << n << ") == "
                  << sqrt_n << " != " << approx_sqrt_n << ']'
                  << std::endl << std::endl;
      }
    }

    if ((n & 0xFFFFFFF) == 0) {
      std::cout << ' ' << n;
      std::cout.flush();
    }

    assert(sqrt_n == (sqrt_n % 4294967296u));
    assert(sqrt_n*sqrt_n <= n);
    assert(n < (sqrt_n + 1)*(sqrt_n + 1));

    assert(sqrt_n == floor_sqrt64(n));
  }
  std::cout << std::endl;


  std::cout << "Check on 64 bits (do NOT expect finish that!)...";

  const uint64_t last64 = (static_cast<uint64_t>(-1));

  assert(floor_sqrt64(last64) == 4294967295u);

  all_correct = true;

  for (uint64_t n = 0; n < last64; ++n) {
    const uint64_t sqrt_n = floor_sqrt64(n);

    if (all_correct) {
      const uint64_t approx_sqrt_n = floor(sqrt(static_cast<double>(n)));

      if (sqrt_n != approx_sqrt_n) {
        all_correct = false;
        std::cout << std::endl << std::endl
                  << "[!!! first error on exact sqrt(" << n << ") == "
                  << sqrt_n << " != " << approx_sqrt_n << ']'
                  << std::endl << std::endl;
      }
    }

    if ((n & 0xFFFFFFFF) == 0) {
      std::cout << ' ' << n;
      std::cout.flush();
    }

    assert(sqrt_n == (sqrt_n % 18446744073709551615ul));

    assert(sqrt_n*sqrt_n <= n);
    assert(n < (sqrt_n + 1)*(sqrt_n + 1));
  }
  std::cout << std::endl;


  return EXIT_SUCCESS;
}
