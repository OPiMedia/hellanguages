# Makefile of hellanguages - integer/floor_sqrt_cpp --- October 30, 2021

PROGS = floor_sqrt_cpp



.SUFFIXES:
.SUFFIXES:	.o

###########
# Options #
###########
CPP      = g++  # https://gcc.gnu.org/
CPPFLAGS =

CXX      = g++
CXXFLAGS = -std=c++11 -pedantic -Wall -Wextra -g

LD      = g++
LDFLAGS =
LIBS    =


CPPCHECK      = cppcheck  # https://cppcheck.sourceforge.io/
CPPCHECKFLAGS = -v --enable=all --report-progress --std=c++11 --platform=unix64

CPPLINT      = cpplint  # https://pypi.org/project/cpplint
CPPLINTFLAGS = --extension hpp,cpp --filter=-readability/braces,-whitespace/newline,-whitespace/line_length,-whitespace/parens

RM    = rm -f
SED   = sed
SHELL = sh



###
# #
###
.PHONY:	all cppcheck cpplint

all:	$(PROGS)

cppcheck:
	@$(CPPCHECK) $(CPPCHECKFLAGS) *.cpp | $(SED) 's/\(files checked.*done\)/\1\n--------------------------------------------------/im'

cpplint:
	@$(CPPLINT) $(CPPLINTFLAGS) *.cpp 2>&1 | $(SED) 's/\(Done processing.*\)/\1\n--------------------------------------------------/im'



#######################
# Compilation options #
#######################
.PHONY:	clang ndebug

clang:  # https://clang.llvm.org/
	$(eval CPP = clang++)
	$(eval CXX = clang++)
	$(eval LD = clang++)

ndebug:
	$(eval CPPFLAGS += -DNDEBUG)
	$(eval CXXFLAGS += -O3)
	$(eval LDFLAGS += -s)



#########
# Rules #
#########
.PRECIOUS:	%.o

%:	%.o
	$(LD) $(LDFLAGS) $^ $(LIBS) -o $@

%.o:	%.cpp
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $< -o $@



#########
# Clean #
#########
.PHONY:	clean distclean overclean

clean:
	$(RM) *.o core *.core

distclean:	clean
	$(RM) $(PROGS)

overclean:	distclean
