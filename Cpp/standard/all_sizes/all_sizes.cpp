/* -*- coding: latin-1 -*- */
/*
 * all_sizes.cpp (February 25, 2014)
 *
 * Print size, minimum and maximum value of each type.
 *
 * See http://www.cplusplus.com/reference/limits/numeric_limits/
 *
 *
 * Piece of hellanguages.
 * https://bitbucket.org/OPiMedia/hellanguages
 *
 * GPLv3 --- Copyright (C) 2014, 2015 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <cstdint>
#include <cstdlib>

#include <iomanip>
#include <iostream>
#include <limits>
#include <string>
#include <typeinfo>


#define COUT_TYPE(type) COUT_TYPE_CAST(type, type)


#define COUT_TYPE_CAST(type, cast_type)                                  \
  std::cout                                                              \
    << std::left << std::setw(18) << #type << " | "                      \
    << std::left << std::setw(4) << typeid(type).name() << " | "         \
    << std::right << std::setw(6) << sizeof(type) << " | "               \
    << std::right << std::setw(6)                                        \
    << std::numeric_limits<type>::digits << " | "                        \
    << std::right << std::setw(20)                                       \
    << static_cast<cast_type>(std::numeric_limits<type>::min()) << " | " \
    << std::right << std::setw(20)                                       \
    << static_cast<cast_type>(std::numeric_limits<type>::max()) << std::endl



int
main() {
#ifdef _LP64
  std::cout << "64 bits: Macro '_LP64' defined";
#else
  std::cout << "Macro '_LP64' NOT defined";
#endif

  std::cout << std::endl
            << std::endl;

  const std::string sep = std::string(19, '-') + '+' + std::string(6, '-')
    + '+' + std::string(8, '-') + '+' + std::string(8, '-')
    + '+' + std::string(22, '-')  + '+' + std::string(22, '-');

  std::cout
    << "Type               | name | sizeof | digits | Minimum              | Maximum" << std::endl  // NOLINT
    << sep << std::endl;

  COUT_TYPE(bool);
  std::cout << sep << std::endl;

  COUT_TYPE_CAST(char, signed int);
  std::cout << sep << std::endl;
  COUT_TYPE(short);  // NOLINT
  COUT_TYPE(int);
  COUT_TYPE(long);  // NOLINT
  COUT_TYPE(long long);  // NOLINT
  std::cout << sep << std::endl;

  COUT_TYPE_CAST(unsigned char, unsigned int);
  std::cout << sep << std::endl;
  COUT_TYPE(unsigned short);  // NOLINT
  COUT_TYPE(unsigned int);
  COUT_TYPE(unsigned long);  // NOLINT
  COUT_TYPE(unsigned long long);  // NOLINT
  std::cout << sep << std::endl;

  COUT_TYPE_CAST(signed char, signed int);
  std::cout << sep << std::endl;
  COUT_TYPE(signed short);  // NOLINT
  COUT_TYPE(signed int);
  COUT_TYPE(signed long);  // NOLINT
  COUT_TYPE(signed long long);  // NOLINT
  std::cout << sep << std::endl;

  COUT_TYPE_CAST(uint8_t, unsigned int);
  COUT_TYPE(uint16_t);
  COUT_TYPE(uint32_t);
  COUT_TYPE(uint64_t);
  COUT_TYPE(uintmax_t);
  std::cout << sep << std::endl;

  COUT_TYPE_CAST(int8_t, signed int);
  COUT_TYPE(int16_t);
  COUT_TYPE(int32_t);
  COUT_TYPE(int64_t);
  COUT_TYPE(intmax_t);
  std::cout << sep << std::endl;

  COUT_TYPE(float);
  COUT_TYPE(double);
  COUT_TYPE(long double);
  std::cout << sep << std::endl;

  COUT_TYPE(void*);

  return EXIT_SUCCESS;
}
