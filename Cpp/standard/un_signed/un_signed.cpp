/* -*- coding: latin-1 -*- */
/*
 * un_signed.cpp (May 11, 2020)
 *
 * Test difference between char, signed char and unsigned char,
 * and for short, int, long.
 *
 *
 * Piece of hellanguages.
 * https://bitbucket.org/OPiMedia/hellanguages
 *
 * GPLv3 --- Copyright (C) 2020 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <cstdlib>

#include <iostream>



/*
  "char is a separate type from the other two and is not compatible with either." (C99, p.35)
  http://www.open-std.org/JTC1/SC22/WG14/www/docs/n1256.pdf
 */

void f(char) { std::cout << "char" << std::endl; }
void f(signed char) { std::cout << "signed char" << std::endl; }
void f(unsigned char) { std::cout << "unsigned char" << std::endl; }


/*
  short and signed short are the *same* type
 */

void f(short) { std::cout << "short" << std::endl; }
// void f(signed short) { std::cout << "signed short" << std::endl; }  // short is same as signed short
void f(unsigned short) { std::cout << "unsigned short" << std::endl; }


/*
  int and signed int are the *same* type
 */

void f(int) { std::cout << "int" << std::endl; }
// void f(signed int) { std::cout << "signed int" << std::endl; }  // int is same as signed int
void f(unsigned int) { std::cout << "unsigned int" << std::endl; }


/*
  long and signed long are the *same* type
 */

void f(long) { std::cout << "long" << std::endl; }
// void f(signed long) { std::cout << "signed long" << std::endl; }  // long is same as signed long
void f(unsigned long) { std::cout << "unsigned long" << std::endl; }



int
main() {
  f(static_cast<char>(42));
  f(static_cast<signed char>(42));  // if f(signed char) not defined, probably call f(int)
  f(static_cast<unsigned char>(42));

  std::cout << std::endl;

  f(static_cast<short>(42));
  f(static_cast<signed short>(42));  // call f(short)
  f(static_cast<unsigned short>(42));

  std::cout << std::endl;

  f(static_cast<long>(42));
  f(static_cast<signed long>(42));  // call f(long)
  f(static_cast<unsigned long>(42));

  std::cout << std::endl;

  f(static_cast<int>(42));
  f(static_cast<signed int>(42));  // call f(int)
  f(static_cast<unsigned int>(42));

  return EXIT_SUCCESS;
}
