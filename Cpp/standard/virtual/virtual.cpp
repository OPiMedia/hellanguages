/* -*- coding: latin-1 -*- */
/*
 * virtual.cpp (May 11, 2020)
 *
 * Test static and dynamic dispatch in subclass.
 *
 *
 * Piece of hellanguages.
 * https://bitbucket.org/OPiMedia/hellanguages
 *
 * GPLv3 --- Copyright (C) 2020 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <cstdlib>

#include <algorithm>
#include <iostream>



class A {
 public:
  void __f() const { std::cout << "A.__f()" << "\t\t"; }
  void _vf() const { std::cout << "A._vf()" << "\t\t"; }
  virtual void v_f() const { std::cout << "A.v_f()" << "\t\t"; }
  virtual void vvf() const { std::cout << "A.vvf()" << std::endl; }
};

class B : public A {
 public:
  void __f() const { std::cout << "B.__f()" << "\t\t"; }
  virtual void _vf() const { std::cout << "B._vf()" << "\t\t"; }
  void v_f() const { std::cout << "B.v_f()" << "\t\t"; }
  virtual void vvf() const { std::cout << "B.vvf()" << std::endl; }
};



int
main() {
  {
    const A aa = A();
    const A ab = B();
    const B bb = B();

    aa.__f();  // A.__f()
    aa._vf();  // A._vf()
    aa.v_f();  // A.v_f()
    aa.vvf();  // A.vvf()

    ab.__f();  // A.__f()
    ab._vf();  // A._vf()
    ab.v_f();  // A.v_f()
    ab.vvf();  // A.vvf()

    bb.__f();  // B.__f()
    bb._vf();  // B._vf()
    bb.v_f();  // B.v_f()
    bb.vvf();  // B.vvf()
  }

  std::cout << std::endl;

  {
    const A& aa = A();
    const A& ab = B();
    const B& bb = B();

    aa.__f();  // A.__f()
    aa._vf();  // A._vf()
    aa.v_f();  // A.v_f()
    aa.vvf();  // A.vvf()

    ab.__f();  // A.__f()
    ab._vf();  // A._vf()
    ab.v_f();  // B.v_f()
    ab.vvf();  // B.vvf()

    bb.__f();  // B.__f()
    bb._vf();  // B._vf()
    bb.v_f();  // B.v_f()
    bb.vvf();  // B.vvf()
  }

  return EXIT_SUCCESS;
}
