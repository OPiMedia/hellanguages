# MIDI - Musical Instrument Digital Interface

Some (maybe) useful links.



## Software documentation

- Advanced Linux Sound Architecture (ALSA)
  <https://www.alsa-project.org/>

    - Debian/ AlsaMidi
      <https://wiki.debian.org/AlsaMidi>
    - List all possible output ports:
      `aplaymidi -l`
    - List connected ports:
        - `aconnect -il`
        - `aconnect -ol`

- FluidSynth - Software synthesizer based on the SoundFont 2 specifications
  <http://www.fluidsynth.org/>

- `modprobe`: add virtual MIDI (Linux) module:
  `modprobe snd-virmidi index=1java`

- RtMidi - "C++ classes that provide a common API for realtime MIDI input/output"
  <https://github.com/thestk/rtmidi>

    - The RtMidi Tutorial
      <https://www.music.mcgill.ca/~gary/rtmidi/>

    - (python-rtmidi <http://trac.chrisarndt.de/code/wiki/python-rtmidi> )

- *Speakerspeaker Sound Troubleshooting Guide*
  <https://docs.google.com/document/d/1iTlJ8BfqXUjaHO__TEdlkvuqB1WLOkGaudngc5SFLMI/edit#heading=h.x2veorvxyqxq>

- Ted's Linux MIDI Guide (Ted Felix)
  <http://www.tedfelix.com/linux/linux-midi.html>

- The Linux MIDI-HOWTO: *HOWTO Use MIDI Sequencers With Softsynths*
  <https://www.tldp.org/HOWTO/MIDI-HOWTO-10.html>

- TiMidity++ - Software synthesizer
  <https://sourceforge.net/projects/timidity/>

    - Restart TiMidity++ daemon:
      `/etc/init.d/timidity restart`



## MIDI documentation

- CECM - Center for Electronic and Computer Music
  <https://cecm.indiana.edu/>

    - *Introduction to Computer Music: Volume One* (Jeffrey Hass)
      <https://cecm.indiana.edu/etext/toc.shtml>
      <https://cmtext.indiana.edu/toc.php>

        - Chapter Three: **MIDI**
          <https://cecm.indiana.edu/etext/MIDI/chapter3_MIDI.shtml>
          <https://cmtext.indiana.edu/MIDI/chapter3_MIDI.php>

- *Introduction to MIDI and Computer Music*
  <https://cecm.indiana.edu/361/>

- MIDI Manufacturers Association - The official source of information about MIDI
  <https://www.midi.org/>

    - *MIDI Messages*
    - <https://www.midi.org/specifications/item/table-1-summary-of-midi-message>

- Multimedia Data (Dave Marshall)
  <http://www.cs.cf.ac.uk/Dave/Multimedia/node141.html>

    - *Introduction to MIDI (Musical Instrument Digital Interface)*
      <http://www.cs.cf.ac.uk/Dave/Multimedia/node155.html>

- Music Software Development - Information, Tools and Tutorials (Dominique Vandenneucker)
  <https://www.music-software-development.com/>

    - *MIDI Tutorial*
      <https://www.music-software-development.com/midi-tutorial.html>

- *Note names, MIDI numbers and frequencies* (Joe Wolfe)
  <http://newt.phys.unsw.edu.au/jw/notes.html>

- Wikipedia: **General MIDI Level 2**
  <https://en.wikipedia.org/wiki/General_MIDI_Level_2>

    - *Note designation in accordance with octave name*
      <https://en.wikipedia.org/wiki/Note#Note_designation_in_accordance_with_octave_name>

    - <https://upload.wikimedia.org/wikipedia/commons/7/7a/NoteNamesFrequenciesAndMidiNumbers.svg>



## MIDI files

- Classical Archives: MIDI
  <https://www.classicalarchives.com/midi.html>



## Maybe to do to me

- Csounds
  <https://www.csounds.com/>

- *Introduction to MIDI programming in Linux* (Craig Stuart Sapp)
  <https://ccrma.stanford.edu/~craig/articles/linuxmidi/>

- Java Sound Resources
  <http://jsresources.sourceforge.net/>

    - FAQ: MIDI Programming
      <http://jsresources.sourceforge.net/faq_midi.html>

        - Examples: MIDI: Synthesizer
          <http://jsresources.sourceforge.net/examples/midi_synthesizer.html>

- *Making Music in the Browser - Web MIDI API* (Keith McMillen Instruments)
  <https://www.keithmcmillen.com/blog/making-music-in-the-browser-web-midi-api/>

- mididings
  <http://das.nasophon.de/mididings/>

- Programming tutorials and source code examples
  <http://www.java2s.com/>

    - MidiChannel: noteOn(int noteNumber, int velocity)
      <http://www.java2s.com/Code/JavaAPI/javax.sound.midi/MidiChannelnoteOnintnoteNumberintvelocity.htm>
