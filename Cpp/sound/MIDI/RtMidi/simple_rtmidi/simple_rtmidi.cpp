/* -*- coding: latin-1 -*- */
/**
 * simple_rtmidi.cpp (January 27, 2015)
 *
 * Simple RtMidi example:
 *   simple_rtmidi [port number]
 *
 * See:
 *   https://github.com/thestk/rtmidi
 *   http://www.music.mcgill.ca/~gary/rtmidi/
 *
 *   https://en.wikipedia.org/wiki/General_MIDI_Level_2
 *
 *
 * Piece of hellanguages.
 * https://bitbucket.org/OPiMedia/hellanguages
 *
 * GPLv3 --- Copyright (C) 2014, 2015 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <cassert>
#include <unistd.h>

#include <iostream>
#include <string>

#include <RtMidi.h>



void
midiout_note_off(RtMidiOut* const midiout,
                 unsigned char channel,
                 unsigned char key_number,
                 unsigned char velocity = 127) {
  assert(midiout != NULL);
  assert(channel <= 15);
  assert(key_number <= 127);
  assert(velocity <= 127);

  static std::vector<unsigned char> message(3);

  message[0] = 0x80 | channel;
  message[1] = key_number;
  message[2] = velocity;
  midiout->sendMessage(&message);
}


void
midiout_note_on(RtMidiOut* const midiout,
                unsigned char channel,
                unsigned char key_number,
                unsigned char velocity = 127) {
  assert(midiout != NULL);
  assert(channel <= 15);
  assert(key_number <= 127);
  assert(velocity <= 127);

  static std::vector<unsigned char> message(3);

  message[0] = 0x90 | channel;
  message[1] = key_number;
  message[2] = velocity;
  midiout->sendMessage(&message);
}


void
midiout_program_change(RtMidiOut* const midiout,
                       unsigned char channel,
                       unsigned char program_number) {
  assert(midiout != NULL);
  assert(channel <= 15);
  assert(program_number <= 127);

  static std::vector<unsigned char> message(2);

  message[0] = 0xC0 | channel;
  message[1] = program_number;
  midiout->sendMessage(&message);
}



int
main(int argc, const char* const argv[]) {
  std::cout << "Usage: simple_rtmidi [port number]" << std::endl
            << std::endl
            << "RtMidi version: " << RtMidi::getVersion() << std::endl;


  // List available compiled MIDI APIs
  std::cout << "Available compiled MIDI APIs:" << std::endl;

  std::vector<RtMidi::Api> apis;

  RtMidi::getCompiledApi(apis);

  for (auto it(apis.begin()); it != apis.end(); ++it) {
    switch (*it) {
    case RtMidi::LINUX_ALSA:
      std::cout << "  - LINUX_ALSA" << std::endl;

      break;

    case RtMidi::UNIX_JACK:
      std::cout << "  - UNIX_JACK" << std::endl;

      break;

    case RtMidi::MACOSX_CORE:
      std::cout << "  - MACOSX_CORE" << std::endl;

      break;

    case RtMidi::WINDOWS_MM:
      std::cout << "  - WINDOWS_MM" << std::endl;

      break;

    case RtMidi::RTMIDI_DUMMY:
      std::cout << "  - RTMIDI_DUMMY" << std::endl;

      break;

    case RtMidi::UNSPECIFIED:
      std::cout << "  - UNSPECIFIED" << std::endl;

      break;

    default:
      std::cout << "  - unknow!" << std::endl;

      break;
    }
  }


  // Create MIDI output
  RtMidiOut* midiout;

  try {
    midiout = new RtMidiOut();
  }
  catch (RtMidiError &error) {
    error.printMessage();

    exit(EXIT_FAILURE);
  }


  // List all ports
  std::cout << "# ports: " << midiout->getPortCount() << std::endl;
  for (unsigned int i(0); i < midiout->getPortCount(); ++i) {
    std::cout << "  " << i << ": " << midiout->getPortName(i) << std::endl;
  }

  unsigned int port(0);

  if (argc > 1) {
    try {
      const int i(std::stoi(argv[1]));

      if ((0 <= i) && ((unsigned int)i < midiout->getPortCount())) {
        port = i;
      }
    }
    catch (std::exception &error) {}
  }

  try {
    midiout->openPort(port);
  }
  catch (RtMidiError &error) {
    error.printMessage();

    delete midiout;

    exit(EXIT_FAILURE);
  }

  std::cout << "Port " << port << " is" << (midiout->isPortOpen()
                                            ? ""
                                            : " NOT") << " open" << std::endl;


  // Change instruments
  std::cout << "Change instruments" << std::endl;
  midiout_program_change(midiout, 0, 32);  // Channel 0 - Instrument 32: Acoustic Bass
  midiout_program_change(midiout, 1, 1);   // Channel 1 - Instrument 1: Bright Acoustic Piano
  sleep(3);


  // Play notes
  std::cout << "Play..." << std::endl;
  midiout_note_on(midiout, 0, 60);  // C note
  sleep(1);
  midiout_note_off(midiout, 0, 60);

  midiout_note_on(midiout, 1, 61);  // D note
  sleep(1);
  midiout_note_off(midiout, 1, 61);

  midiout_note_on(midiout, 1, 62);  // E note
  sleep(1);
  midiout_note_off(midiout, 1, 62);

  midiout_note_on(midiout, 1, 63);  // F note
  sleep(1);
  midiout_note_off(midiout, 1, 63);

  sleep(3);


  // Close
  midiout->closePort();
  std::cout << "-- end --" << std::endl;

  delete midiout;

  return EXIT_SUCCESS;
}
