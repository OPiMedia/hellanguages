# Makefile of hellanguages --- July 4, 2020

.SUFFIXES:

CHECKTXT = checkTxt.pl
SHELL    = sh



########
# Test #
########
.PHONY: checkTxt

checkTxt:
	$(CHECKTXT) +lisible -put +questions \
-E~$$ -E^_img/ \
\
'-E^C/benchmark/iteration/(estimation|results)__.+\.txt$$' \
\
-FC/integer/floor_sqrt_c/floor_sqrt_c__gcc_6_3.log \
-E^C/standard/cmps/cmp_.+?\.a -EC/standard/mul2_div2/.+?\.a$$ \
\
-FCpp/integer/floor_sqrt_cpp/floor_sqrt_cpp__gcc_6_3.log \
-FCpp/standard/virtual/out.txt \
-FCpp/template/lint.log \
\
-Fcomparative/divisions/divisions.tsv \
\
-E^Java/.+\.class$$ \
-E^Java/standard/imports/.+?/Test\.class -EJava/standard/imports/.+?/Test__decompiled\.java$$ \
-FJava/sound/MIDI/SimpleMidiSynthesis/SimpleMidiSynthesis.class \
\
-'E^LaTeX/template/template\.(sty|tex)$$' \
\
-FPython/standard/multiple_inheritance/multiple_inheritance_3.txt \
\
-'E^Scala/standard/assert/assert__(dis|en)abled.jar$$' \
-'E^Scala/standard/assert/run_.+\.log$$' \
\
* */* */*/* */*/*/* */*/*/*/* */*/*/*/*/*
