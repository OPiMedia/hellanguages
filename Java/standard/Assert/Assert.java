/**
 * Assert.java (November 27, 2018)
 *
 * Simple program with a failing assertion,
 * to test execution.
 *
 * Piece of hellanguages.
 * https://bitbucket.org/OPiMedia/hellanguages
 *
 * GPLv3 --- Copyright (C) 2018 Olivier Pirson
 * http://www.opimedia.be/
 */
public class Assert {
  public static void main(String args[]) {
    System.out.println("Before assert");
    System.out.flush();

    assert(false);

    System.out.println("After assert");
    System.out.flush();
  }
}
