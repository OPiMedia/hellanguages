#!/bin/sh

echo '===== java Assert (assertion disabled by default) ====='
java Assert

echo
echo '===== java -da Assert ====='
java -da Assert

echo
echo '===== java -ea Assert ====='
java -ea Assert
