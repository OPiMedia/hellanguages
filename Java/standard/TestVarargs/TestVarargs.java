/**
 * TestVarargs.java (November 25, 2014)
 *
 * Test overload on functions with varargs.
 *
 *
 * Piece of hellanguages.
 * https://bitbucket.org/OPiMedia/hellanguages
 *
 * GPLv3 --- Copyright (C) 2014 Olivier Pirson
 * http://www.opimedia.be/
 */
public class TestVarargs {
    public static void foo() {
        System.out.println("foo()");
    }

    public static void foo(int... params) {
        System.out.println("foo(int... params): params.length=" + params.length);
    }

    public static void foo(int a) {
        System.out.println("foo(int a)");
    }



    public static void main(String args[]) {
        foo();                        // Print foo()
        foo(0);                       // Print foo(int a)
        foo(0, 1);                    // Print foo(int... params): params.length=2
        foo(0, 1, 2);                 // Print foo(int... params): params.length=3
        foo(new int[] {0, 1, 2, 3});  // Print foo(int... params): params.length=4
    }
}
