/**
 * B.java (February 3, 2015)
 *
 * Second of two simple classes with a main method.
 *
 *
 * Piece of hellanguages.
 * https://bitbucket.org/OPiMedia/hellanguages
 *
 * GPLv3 --- Copyright (C) 2015 Olivier Pirson
 * http://www.opimedia.be/
 */
public class B {
    public static void print() {
        System.out.println("B.print()");
    }


    public static void main(String args[]) {
        System.out.println("B.main()");

        A.print();
        B.print();
    }
}
