/**
 * A.java (February 3, 2015)
 *
 * First of two simple classes with a main method.
 *
 *
 * Piece of hellanguages.
 * https://bitbucket.org/OPiMedia/hellanguages
 *
 * GPLv3 --- Copyright (C) 2015 Olivier Pirson
 * http://www.opimedia.be/
 */
public class A {
    public static void print() {
        System.out.println("A.print()");
    }


    public static void main(String args[]) {
        System.out.println("A.main()");

        A.print();
        B.print();
    }
}
