/**
 * Test.java (November 11, 2015)
 *
 * Compare bytecode of different from to access class.
 * Import only the good class version.
 *
 *
 * Piece of hellanguages.
 * https://bitbucket.org/OPiMedia/hellanguages
 *
 * GPLv3 --- Copyright (C) 2015 Olivier Pirson
 * http://www.opimedia.be/
 */

import java.util.ArrayList;

public class Test {
    public static void main(String args[]) {
        ArrayList array = new ArrayList();

        System.out.println(array.size());
    }
}
