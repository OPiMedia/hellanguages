/**
 * Test.java (November 11, 2015)
 *
 * Compare bytecode of different from to access class.
 * Direction version (without import).
 *
 *
 * Piece of hellanguages.
 * https://bitbucket.org/OPiMedia/hellanguages
 *
 * GPLv3 --- Copyright (C) 2015 Olivier Pirson
 * http://www.opimedia.be/
 */
public class Test {
    public static void main(String args[]) {
        java.util.ArrayList array = new java.util.ArrayList();

        System.out.println(array.size());
    }
}
