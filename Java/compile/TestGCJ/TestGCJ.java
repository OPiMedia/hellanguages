/**
 * TestGCJ.java (November 25, 2014)
 *
 * Very simple Java program to test compilation with GCJ - GNU Compiler for Java
 * https://gcc.gnu.org/java/
 *
 * Produce TestGCJ.class:
 *   gcj TestGCJ.java -C
 *
 * Produce TestGCJ native executable:
 *   gcj --main=TestGCJ TestGCJ.java -o TestGCJ
 *
 *
 * Piece of hellanguages.
 * https://bitbucket.org/OPiMedia/hellanguages
 *
 * GPLv3 --- Copyright (C) 2014 Olivier Pirson
 * http://www.opimedia.be/
 */
public class TestGCJ {

    public static void main(String args[]) {
        System.out.println("Very simple Java program to test compilation with GCJ - GNU Compiler for Java");
    }

}
