/**
 * SimpleMidiSynthesis.java (April 19, 2020)
 *
 * Simple example of MIDI synthesis with the standard javax.sound.midi package.
 *
 * Usage:
 *   java SimpleMidiSynthesis [--list-instruments] [device number]
 *
 *
 * References:
 *
 * * Package javax.sound.midi:
 *   * https://docs.oracle.com/javase/8/docs/api/javax/sound/midi/package-summary.html
 *   * https://docs.oracle.com/en/java/javase/14/docs/api/java.desktop/javax/sound/midi/package-summary.html
 *
 * * Midi Synthesis:
 *   https://patater.com/gbaguy/javamidi.htm
 *
 * * General MIDI Level 2:
 *   https://en.wikipedia.org/wiki/General_MIDI_Level_2
 *
 * * Note designation in accordance with octave name
 *   https://en.wikipedia.org/wiki/Musical_note#Note_designation_in_accordance_with_octave_name
 *
 * * https://bitbucket.org/OPiMedia/hellanguages/src/master/Cpp/sound/MIDI/
 *
 *
 * Piece of hellanguages.
 * https://bitbucket.org/OPiMedia/hellanguages
 *
 * GPLv3 --- Copyright (C) 2014, 2020 Olivier Pirson
 * http://www.opimedia.be/
 */

import java.lang.Thread;

import javax.sound.midi.Instrument;
import javax.sound.midi.MidiChannel;
import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Soundbank;
import javax.sound.midi.Synthesizer;



public class SimpleMidiSynthesis {
    public static void main(String[] args) {
        final MidiDevice.Info[] infos = MidiSystem.getMidiDeviceInfo();

        int device_number = 0;
        boolean list_instruments = false;

        System.out.println("Usage: java SimpleMidiSynthesis [--list-instruments] [device number]\n");

        for (int i = 0; i < args.length; ++i) {
            if (args[i].equals("--list-instruments")) {
                list_instruments = true;
            }
            else {
                try {
                    device_number = Integer.parseInt(args[i]);
                }
                catch (Exception e) {}

                if (device_number < 0 || device_number >= infos.length) {
                    device_number = 0;
                }
            }
        }


        // Print all devices
        for (int i = 0; i < infos.length; ++i) {
            System.out.println("Device " + i + ": " + infos[i]);
        }


        // Open device
        MidiDevice device = null;

        try {
            device = MidiSystem.getMidiDevice(infos[device_number]);
            device.open();
            System.out.println("Device opened " + device_number + ": " + infos[device_number]);
        }
        catch (Exception e) {
            System.out.println("Device NOT opened!");
            e.printStackTrace();

            System.exit(1);
        }


        // Open synthesizer
        Synthesizer synthetiser = null;

        try {
            synthetiser = MidiSystem.getSynthesizer();
            synthetiser.open();
            System.out.println("Synthesizer loaded");
        }
        catch (MidiUnavailableException e) {
            System.out.println("Synthesizer NOT loaded OR NOT opened!");
            e.printStackTrace();

            System.exit(1);
        }


        // Get default soundbank
        final Soundbank soundbank = synthetiser.getDefaultSoundbank();

        if (soundbank != null) {
            System.out.println("Default soundbank ok");
        }
        else {
            System.out.println("Default soundbank KO!");
        }


        // Print all instruments list
        final Instrument[] instruments = soundbank.getInstruments();

        if (list_instruments) {
            for (int i = 0; i < instruments.length; ++i) {
                System.out.println("Instrument " + i + ": " + instruments[i]);
            }
        }


        // Load all instruments
        if (synthetiser.loadAllInstruments(soundbank)) {
            System.out.println("All instruments loaded");
        }
        else {
            System.out.println("All instruments NOT loaded!");
        }


        // Get all channels
        final MidiChannel[] channels = synthetiser.getChannels();

        for (int i = 0; i < channels.length; ++i) {
            System.out.println("Channel " + i + ": " + channels[i]);  // channel maybe is null
        }


        final int velocity = 127;  // usually note's volume and/or brightness


        sleep(1000);

        // Play a note
        channels[0].programChange(0);  // Acoustic Grand Piano
        System.out.println("Current program of channel 0: " + instruments[channels[0].getProgram()]);
        channels[0].noteOn(70, velocity);  // A4

        sleep(1000);

        // Play a note
        channels[1].programChange(12);  // Marimba
        System.out.println("Current program of channel 1: " + instruments[channels[1].getProgram()]);
        channels[1].noteOn(70, velocity);  // A4

        sleep(1000);

        // Play a note
        channels[2].programChange(79);  // Sitar
        System.out.println("Current program of channel 2: " + instruments[channels[2].getProgram()]);
        channels[2].noteOn(70, velocity);  // A4

        sleep(3000);


        // Close
        synthetiser.close();
        device.close();
    }


    public static void sleep(int ms) {
        System.out.print("Sleep " + ms + "ms...");

        try {
            Thread.sleep(ms);
        } catch (InterruptedException ie) {}

        System.out.println(" end");
    }
}
